<?php

try {

    // autoloader
    $loader = new Phalcon\Loader();
    $loader->registerDirs(array(
        '../app/controllers/',
        '../app/models/'
    ))->register();

    // create DI
    $di = new Phalcon\DI\FactoryDefault();
    $di->set('view', function() {
        $view = new Phalcon\Mvc\View();
        $view->setViewsDir('../app/views');
        $view->registerEngines(array(
            '.volt' => 'Phalcon\Mvc\View\Engine\Volt'
        ));
        return $view;
    });

    // handle request
    $app = new Phalcon\Mvc\Application($di);
    echo $app->handle()->getContent();

} catch(Phalcon\Exception $e) {
    echo "PhalconException: ", $e->getMessage();
}
